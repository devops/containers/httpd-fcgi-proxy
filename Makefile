SHELL = /bin/bash

build_tag ?= httpd-fcgi-proxy

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) ./src

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	./test.sh
