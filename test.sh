#!/bin/bash

container="httpd-fcgi-proxy-${CI_COMMIT_SHORT_SHA:-candidate}"

docker-compose up -d

code=1
SECONDS=0
while [[ $SECONDS -lt 60 ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $container)
    echo $status

    case $status in
        healthy)
	    echo "SUCCESS"
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep 5
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

docker-compose down

exit $code
